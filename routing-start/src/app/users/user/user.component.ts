import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: {id: number, name: string};

  constructor(private route : ActivatedRoute) { }

  ngOnInit() {
    this.user =  {
      id : this.route.snapshot.params['id'],
      name : this.route.snapshot.params['name']
    }

    // 當params有變動時，而且component又已經建立好時，可以透過params這個observable去接收新的data並更新到user中
    this.route.params.subscribe(
      (params:Params)=> {
        this.user.id = params['id'];
        this.user.name = params['name'];
      }
    )

  }

}
