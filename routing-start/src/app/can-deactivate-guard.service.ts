import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';

// 創建一個interface，內容
export interface canComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
  providedIn: 'root'
})

// CanDeactivate會需要指定一個component來讓這個guard去處理指定的component離開時的狀況
export class CanDeactivateGuardService implements CanDeactivate<canComponentDeactivate>{

  constructor() { }

  canDeactivate(component: canComponentDeactivate,
                currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot,
                nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // 這邊回傳了上面interface指定的一個function，這個function會傳回一個observable,
    // 到這邊很明顯的，上面那個interface就是要給需要使用這個guard的component去implements的
    return component.canDeactivate();
  }


}
