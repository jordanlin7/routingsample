import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedIn = false;

  isAutheticated() {
    // 建立一個delay 800ms回傳資料的observable
    const source$ = of(this.loggedIn).pipe(
      delay(800)
    )
    return source$;
  }

  login() {
    this.loggedIn = true;
  }

  logout() {
    this.loggedIn = false;
  }

  constructor() { }
}
